# OpenML dataset: GesturePhaseSegmentationProcessed

https://www.openml.org/d/4538

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Renata Cristina Barros Madeo (Madeo","R. C. B.)  Priscilla Koch Wagner (Wagner","P. K.)  Sarajane Marques Peres (Peres","S. M.)  {renata.si","priscilla.wagner","sarajane} at usp.br  http://each.uspnet.usp.br/sarajane/  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/gesture+phase+segmentation)  
**Please cite**: Please refer to the [Machine Learning Repository's citation policy](https://archive.ics.uci.edu/ml/citation_policy.html). Additionally, the authors require a citation to one or more publications from those cited as relevant papers.  

Creators: 
Renata Cristina Barros Madeo (Madeo, R. C. B.) 
Priscilla Koch Wagner (Wagner, P. K.) 
Sarajane Marques Peres (Peres, S. M.) 
{renata.si, priscilla.wagner, sarajane} at usp.br 
http://each.uspnet.usp.br/sarajane/ 

Donor: 
University of Sao Paulo - Brazil

Data Set Information:

The dataset is composed by features extracted from 7 videos with people gesticulating, aiming at studying Gesture Phase Segmentation. 
Each video is represented by two files: a raw file, which contains the position of hands, wrists, head and spine of the user in each frame; and a processed file, which contains velocity and acceleration of hands and wrists. See the data set description for more information on the dataset.

Attribute Information:

Raw files: 18 numeric attributes (double), a timestamp and a class attribute (nominal). 
Processed files: 32 numeric attributes (double) and a class attribute (nominal). 
A feature vector with up to 50 numeric attributes can be generated with the two files mentioned above.

This is the processed data set with the following feature description:

   Processed files:

   1. Vectorial velocity of left hand (x coordinate)
   2. Vectorial velocity of left hand (y coordinate)
   3. Vectorial velocity of left hand (z coordinate)
   4. Vectorial velocity of right hand (x coordinate)
   5. Vectorial velocity of right hand (y coordinate)
   6. Vectorial velocity of right hand (z coordinate)
   7. Vectorial velocity of left wrist (x coordinate)
   8. Vectorial velocity of left wrist (y coordinate)
   9. Vectorial velocity of left wrist (z coordinate)
   10. Vectorial velocity of right wrist (x coordinate)
   11. Vectorial velocity of right wrist (y coordinate)
   12. Vectorial velocity of right wrist (z coordinate)
   13. Vectorial acceleration of left hand (x coordinate)
   14. Vectorial acceleration of left hand (y coordinate)
   15. Vectorial acceleration of left hand (z coordinate)
   16. Vectorial acceleration of right hand (x coordinate)
   17. Vectorial acceleration of right hand (y coordinate)
   18. Vectorial acceleration of right hand (z coordinate)
   19. Vectorial acceleration of left wrist (x coordinate)
   20. Vectorial acceleration of left wrist (y coordinate)
   21. Vectorial acceleration of left wrist (z coordinate)
   22. Vectorial acceleration of right wrist (x coordinate)
   23. Vectorial acceleration of right wrist (y coordinate)
   24. Vectorial acceleration of right wrist (z coordinate)
   25. Scalar velocity of left hand
   26. Scalar velocity of right hand
   27. Scalar velocity of left wrist
   28. Scalar velocity of right wrist
   29. Scalar velocity of left hand
   30. Scalar velocity of right hand
   31. Scalar velocity of left wrist
   32. Scalar velocity of right wrist
   33. phase:
       - D (rest position, from portuguese "descanso")
       - P (preparation)
       - S (stroke)
       - H (hold)
       - R (retraction)

Relevant Papers:

1. Madeo, R. C. B. ; Lima, C. A. M. ; PERES, S. M. . Gesture Unit Segmentation using Support Vector Machines: Segmenting 
Gestures from Rest Positions. In: Symposium on Applied Computing (SAC), 2013, Coimbra. Proceedings of the 28th Annual 
ACM Symposium on Applied Computing (SAC), 2013. p. 46-52. 
* In this paper, the videos A1 and A2 were studied. 

2. Wagner, P. K. ; PERES, S. M. ; Madeo, R. C. B. ; Lima, C. A. M. ; Freitas, F. A. . Gesture Unit Segmentation Using 
Spatial-Temporal Information and Machine Learning. In: 27th Florida Artificial Intelligence Research Society Conference 
(FLAIRS), 2014, Pensacola Beach. Proceedings of the 27th Florida Artificial Intelligence Research Society Conference 
(FLAIRS). Palo Alto : The AAAI Press, 2014. p. 101-106. 
* In this paper, the videos A1, A2, A3, B1, B3, C1 and C3 were studied. 

3. Madeo, R. C. B.. Support Vector Machines and Gesture Analysis: incorporating temporal aspects (in Portuguese). Master 
Thesis - Universidade de Sao Paulo, Sao Paulo Researcher Foundation. 2013. 
* In this document, the videos named B1 and B3 in the document correspond to videos C1 and C3 in this dataset. Only 
five videos were explored in this document: A1, A2, A3, C1 and C3. 

4. Wagner, P. K. ; Madeo, R. C. B. ; PERES, S. M. ; Lima, C. A. M. . Segmenta&Atilde;&sect;ao de Unidades Gestuais com Multilayer 
Perceptrons (in Portuguese). In: Encontro Nacional de Inteligencia Artificial e Computacional (ENIAC), 2013, Fortaleza. 
Anais do X Encontro Nacional de Inteligencia Artificial e Computacional (ENIAC), 2013. 
* In this paper, the videos A1, A2 and A3 were studied.



Citation Request:

Please refer to the Machine Learning Repository's citation policy. 
Additionally, the authors require a citation to one or more publications from those cited as relevant papers.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4538) of an [OpenML dataset](https://www.openml.org/d/4538). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4538/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4538/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4538/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

